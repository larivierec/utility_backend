package com.geek.opensource.utility_backend.configuration.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.vault.annotation.VaultPropertySource;

import javax.sql.DataSource;

@Configuration
@VaultPropertySource("secret/services/utility_backend/mysql_credentials")
public class DatasourceConfiguration {

    private final Environment env;

    @Autowired
    public DatasourceConfiguration(Environment environment) {
        env = environment;
    }

    @Bean
    public DataSource dataSourceBean() {
        DataSourceBuilder builder = DataSourceBuilder.create();

        builder.username(env.getRequiredProperty("mysql_username"));
        builder.password(env.getRequiredProperty("mysql_password"));
        builder.url(env.getRequiredProperty("mysql_url"));
        return builder.build();
    }
}
