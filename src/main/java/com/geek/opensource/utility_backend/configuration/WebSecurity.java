package com.geek.opensource.utility_backend.configuration;

import com.geek.opensource.utility_backend.SecurityConstants;
import com.geek.opensource.utility_backend.filter.JWTAuthenticationFilter;
import com.geek.opensource.utility_backend.filter.JWTAuthorizationFilter;
import com.geek.opensource.utility_backend.service.UserDetailsServiceImpl;
import com.geek.opensource.utility_backend.service.VaultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final Logger logger = LoggerFactory.getLogger(WebSecurity.class);

    private final UserDetailsServiceImpl service;
    private final BCryptPasswordEncoder encoder;
    private final VaultService vaultService;

    @Autowired
    public WebSecurity(UserDetailsServiceImpl service, BCryptPasswordEncoder coder, VaultService vaultService) throws Exception {
        this.service = service;
        this.encoder = coder;
        this.vaultService = vaultService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String jwtSecret = vaultService.readSecret("secret/services/utility_backend/jwt").getRequiredData().get("secret").toString();

        http.cors().and().csrf().disable()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(), jwtSecret))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtSecret))
                .authorizeRequests(authorizeRequests -> authorizeRequests
                        .antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL).permitAll()
                        .antMatchers(HttpMethod.POST, SecurityConstants.LOGIN_URL).permitAll()
                        .antMatchers(HttpMethod.POST, "/home").permitAll()
                        .anyRequest().authenticated()
                )
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(service).passwordEncoder(encoder);
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
}
