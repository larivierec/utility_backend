package com.geek.opensource.utility_backend.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AsyncConfiguration {

    @Bean(name = "asyncExecutor")
    public Executor asyncExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(Math.round(java.lang.Thread.activeCount() / 2));
        executor.setMaxPoolSize(java.lang.Thread.activeCount());
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("SpringProjectThread-");
        executor.initialize();
        return executor;
    }
}
