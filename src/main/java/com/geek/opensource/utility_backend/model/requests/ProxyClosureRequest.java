package com.geek.opensource.utility_backend.model.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProxyClosureRequest {
    private final int port;

    @JsonCreator
    public ProxyClosureRequest(@JsonProperty("proxy_port") final Integer proxyPort) {
        port = proxyPort;
    }

    public int getPort() {
        return port;
    }
}
