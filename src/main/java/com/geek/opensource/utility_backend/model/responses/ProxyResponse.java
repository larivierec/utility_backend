package com.geek.opensource.utility_backend.model.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProxyResponse {

    @JsonProperty("proxy_port")
    public int proxyPort;
    @JsonProperty("dns_name")
    public String dnsName;
}
