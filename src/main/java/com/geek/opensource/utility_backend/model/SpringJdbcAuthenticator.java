package com.geek.opensource.utility_backend.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import sockslib.common.Credentials;
import sockslib.server.Session;
import sockslib.server.UsernamePasswordAuthenticator;

import java.util.ArrayList;

@Component
public class SpringJdbcAuthenticator extends UsernamePasswordAuthenticator {
    private final Logger logger = LoggerFactory.getLogger(SpringJdbcAuthenticator.class);

    private final AuthenticationManager manager;

    @Autowired
    public SpringJdbcAuthenticator(final AuthenticationManager authManager) {
        manager = authManager;
    }

    @Override
    public void doAuthenticate(Credentials credentials, Session session) throws sockslib.common.AuthenticationException {
        if (manager != null) {
            try {
                Authentication result = manager.authenticate(new UsernamePasswordAuthenticationToken(credentials.getUserPrincipal(), credentials.getPassword(), new ArrayList<>()));
                authenticationSuccess(session, result);
            } catch (AuthenticationException e) {
                logger.error(e.getMessage());
                authenticationFailed(session);
            }
        }
    }

    private void authenticationSuccess(Session session, Authentication user) {
        session.setAttribute(USER_KEY, user);
    }
}
