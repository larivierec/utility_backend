package com.geek.opensource.utility_backend.service;

import com.geek.opensource.utility_backend.data.nordvpn.NordVpnServerData;
import com.geek.opensource.utility_backend.model.responses.ProxyResponse;
import com.geek.opensource.utility_backend.model.SpringJdbcAuthenticator;
import com.geek.opensource.utility_backend.configuration.vault.VpnCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sockslib.client.Socks5;
import sockslib.client.SocksProxy;
import sockslib.common.UsernamePasswordCredentials;
import sockslib.common.methods.SocksMethod;
import sockslib.common.methods.UsernamePasswordMethod;
import sockslib.server.SocksProxyServer;
import sockslib.server.SocksServerBuilder;
import sockslib.server.listener.LoggingListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class ProxyService {
    private Logger logger = LoggerFactory.getLogger(ProxyService.class);
    private final String NORDVPN_SECRET_PATH = "secret/nordvpn/credentials";

    private final VpnInterrogatorService vpnInterrogatorService;
    private final VpnCredentials credentials;
    private final SpringJdbcAuthenticator jdbcAuthenticator;

    private Map<Integer, SocksProxyServer> currentConnections;
    private final int MAX_ALLOWED_CONNECTIONS = 4;

    @Autowired
    public ProxyService(final VpnInterrogatorService service, final VaultService vaultService, SpringJdbcAuthenticator springJdbcAuthenticator) {
        vpnInterrogatorService = service;
        jdbcAuthenticator = springJdbcAuthenticator;
        credentials = vaultService.readSecret(NORDVPN_SECRET_PATH, VpnCredentials.class).getRequiredData();
        currentConnections = new HashMap<>();
    }


    /**
     *
     * @param countryCode 2 letter country code
     * @param vpnType P2P
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public ProxyResponse createProxy(String countryCode, String vpnType) throws IOException, ExecutionException, InterruptedException {
        ProxyResponse returnedProxy = new ProxyResponse();
        if(currentConnections.size() < MAX_ALLOWED_CONNECTIONS) {
            List<NordVpnServerData> data = vpnInterrogatorService.getNordVpnServerData().get().stream()
                    .filter(element -> element.getLoad() <= 50)
                    .filter(element -> element.getFlag().equalsIgnoreCase(countryCode))
                    .filter(element -> element.getSearch_keywords().contains(vpnType)
                            && element.getFeatures().isSocks())
                    .sorted(Comparator.comparingInt(NordVpnServerData::getLoad))
                    .collect(Collectors.toList());

            if (data.size() > 0) {
                List<SocksMethod> methods = new ArrayList<>();
                methods.add(new UsernamePasswordMethod());

                SocksProxy outgoingProxy = new Socks5(data.get(0).getDomain(), 1080,
                        new UsernamePasswordCredentials(credentials.user, credentials.password))
                        .setAcceptableMethods(methods);

                int startPortNumber = 1080;
                SocksProxyServer proxyServer = SocksServerBuilder.newSocks5ServerBuilder()
                        .setSocksMethods(new UsernamePasswordMethod(jdbcAuthenticator))
                        .addSessionListener("logging", new LoggingListener())
                        .setBindPort(startPortNumber + currentConnections.size())
                        .setProxy(outgoingProxy)
                        .build();

                try {
                    proxyServer.start();
                    returnedProxy.proxyPort = startPortNumber + currentConnections.size();
                    currentConnections.put(startPortNumber + currentConnections.size(), proxyServer);
                } catch (Exception e) {
                    logger.error(e.toString());
                }
            }
        }
        return returnedProxy;
    }

    public void stopProxy(int port) {
        SocksProxyServer server = currentConnections.get(port);
        if (server != null) {
            server.shutdown();
            currentConnections.remove(port);
        }
    }
}
