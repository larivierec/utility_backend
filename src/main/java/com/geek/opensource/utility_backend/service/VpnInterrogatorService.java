package com.geek.opensource.utility_backend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.geek.opensource.utility_backend.data.nordvpn.NordVpnServerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class VpnInterrogatorService {
    private Logger logger = LoggerFactory.getLogger(VpnInterrogatorService.class);
    private RestTemplate serverApiRequest;
    private URI nordvpnURI;

    public VpnInterrogatorService() {
        try {
            nordvpnURI = new URI("https://nordvpn.com/api/server");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        serverApiRequest = new RestTemplate();
    }

    @Async("asyncExecutor")
    public CompletableFuture<List<NordVpnServerData>> getNordVpnServerData() throws IOException {
        logger.info("Requesting data from NordVpn servers...");
        String jsonData = serverApiRequest.getForObject(nordvpnURI, String.class);
        ObjectMapper mapper = new ObjectMapper();
        TypeFactory typeFactory = mapper.getTypeFactory();
        CollectionType collectionType = typeFactory.constructCollectionType(List.class, NordVpnServerData.class);

        InputStream targetStream = new ByteArrayInputStream(jsonData.getBytes());
        List<NordVpnServerData> nordVpnServers = mapper.readValue(targetStream, collectionType);

        return CompletableFuture.completedFuture(nordVpnServers);
    }
}
