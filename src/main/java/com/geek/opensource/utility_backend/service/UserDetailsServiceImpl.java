package com.geek.opensource.utility_backend.service;

import com.geek.opensource.utility_backend.dao.IUserDao;
import com.geek.opensource.utility_backend.dao.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private IUserDao IUserDao;

    public UserDetailsServiceImpl(IUserDao dao) {
        this.IUserDao = dao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = IUserDao.findByEmail(email);
        if(user == null) {
            throw new UsernameNotFoundException(email);
        }

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), emptyList());
    }
}
