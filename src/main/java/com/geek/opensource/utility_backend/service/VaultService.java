package com.geek.opensource.utility_backend.service;

import com.geek.opensource.utility_backend.configuration.vault.VaultConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;
import org.springframework.vault.support.VaultResponseSupport;

@Service
public class VaultService {
    private final VaultTemplate vaultTemplate;

    @Autowired
    public VaultService(final VaultConfiguration vaultConfiguration) {
        this.vaultTemplate = new VaultTemplate(vaultConfiguration.vaultEndpoint(), vaultConfiguration.clientAuthentication());
    }

    public VaultResponse readSecret(String path) {
        return vaultTemplate.read(path);
    }

    public <T> VaultResponseSupport<T> readSecret(String path, Class<T> clazz) {
        return vaultTemplate.read(path, clazz);
    }
}
