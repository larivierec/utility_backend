package com.geek.opensource.utility_backend.data.nordvpn;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NordVpnGeoPoint {
    @JsonProperty("lat")
    private double lat;

    @JsonProperty("long")
    private double lon;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
