package com.geek.opensource.utility_backend.data.nordvpn;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class NordVpnFeatures {
    private boolean ikev2;
    private boolean openvpn_udp;
    private boolean openvpn_tcp;
    private boolean socks;
    private boolean proxy;
    private boolean pptp;
    private boolean l2tp;
    private boolean openvpn_xor_udp;
    private boolean openvpn_xor_tcp;
    private boolean proxy_cybersec;
    private boolean proxy_ssl;
    private boolean proxy_ssl_cybersec;
    private boolean ikev2_v6;
    private boolean openvpn_udp_v6;
    private boolean openvpn_tcp_v6;
    private boolean wireguard_udp;
    private boolean openvpn_udp_tls_crypt;
    private boolean openvpn_tcp_tls_crypt;

    public boolean isIkev2() {
        return ikev2;
    }

    public void setIkev2(boolean ikev2) {
        this.ikev2 = ikev2;
    }

    public boolean isOpenvpn_udp() {
        return openvpn_udp;
    }

    public void setOpenvpn_udp(boolean openvpn_udp) {
        this.openvpn_udp = openvpn_udp;
    }

    public boolean isOpenvpn_tcp() {
        return openvpn_tcp;
    }

    public void setOpenvpn_tcp(boolean openvpn_tcp) {
        this.openvpn_tcp = openvpn_tcp;
    }

    public boolean isSocks() {
        return socks;
    }

    public void setSocks(boolean socks) {
        this.socks = socks;
    }

    public boolean isProxy() {
        return proxy;
    }

    public void setProxy(boolean proxy) {
        this.proxy = proxy;
    }

    public boolean isPptp() {
        return pptp;
    }

    public void setPptp(boolean pptp) {
        this.pptp = pptp;
    }

    public boolean isL2tp() {
        return l2tp;
    }

    public void setL2tp(boolean l2tp) {
        this.l2tp = l2tp;
    }

    public boolean isOpenvpn_xor_udp() {
        return openvpn_xor_udp;
    }

    public void setOpenvpn_xor_udp(boolean openvpn_xor_udp) {
        this.openvpn_xor_udp = openvpn_xor_udp;
    }

    public boolean isOpenvpn_xor_tcp() {
        return openvpn_xor_tcp;
    }

    public void setOpenvpn_xor_tcp(boolean openvpn_xor_tcp) {
        this.openvpn_xor_tcp = openvpn_xor_tcp;
    }

    public boolean isProxy_cybersec() {
        return proxy_cybersec;
    }

    public void setProxy_cybersec(boolean proxy_cybersec) {
        this.proxy_cybersec = proxy_cybersec;
    }

    public boolean isProxy_ssl() {
        return proxy_ssl;
    }

    public void setProxy_ssl(boolean proxy_ssl) {
        this.proxy_ssl = proxy_ssl;
    }

    public boolean isProxy_ssl_cybersec() {
        return proxy_ssl_cybersec;
    }

    public void setProxy_ssl_cybersec(boolean proxy_ssl_cybersec) {
        this.proxy_ssl_cybersec = proxy_ssl_cybersec;
    }

    public boolean isIkev2_v6() {
        return ikev2_v6;
    }

    public void setIkev2_v6(boolean ikev2_v6) {
        this.ikev2_v6 = ikev2_v6;
    }

    public boolean isOpenvpn_udp_v6() {
        return openvpn_udp_v6;
    }

    public void setOpenvpn_udp_v6(boolean openvpn_udp_v6) {
        this.openvpn_udp_v6 = openvpn_udp_v6;
    }

    public boolean isOpenvpn_tcp_v6() {
        return openvpn_tcp_v6;
    }

    public void setOpenvpn_tcp_v6(boolean openvpn_tcp_v6) {
        this.openvpn_tcp_v6 = openvpn_tcp_v6;
    }

    public boolean isWireguard_udp() {
        return wireguard_udp;
    }

    public void setWireguard_udp(boolean wireguard_udp) {
        this.wireguard_udp = wireguard_udp;
    }

    public boolean isOpenvpn_udp_tls_crypt() {
        return openvpn_udp_tls_crypt;
    }

    public void setOpenvpn_udp_tls_crypt(boolean openvpn_udp_tls_crypt) {
        this.openvpn_udp_tls_crypt = openvpn_udp_tls_crypt;
    }

    public boolean isOpenvpn_tcp_tls_crypt() {
        return openvpn_tcp_tls_crypt;
    }

    public void setOpenvpn_tcp_tls_crypt(boolean openvpn_tcp_tls_crypt) {
        this.openvpn_tcp_tls_crypt = openvpn_tcp_tls_crypt;
    }
}
