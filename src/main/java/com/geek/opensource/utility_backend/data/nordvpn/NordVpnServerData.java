package com.geek.opensource.utility_backend.data.nordvpn;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown=true)
public class NordVpnServerData {
    private Long id;
    private String ip_address;
    private List<String> search_keywords;
    private List<NordVpnCategory> categories;
    private String name;
    private String domain;
    private float price;
    private String flag;
    private String country;
    private int load;
    private NordVpnGeoPoint location;
    private NordVpnFeatures features;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public List<String> getSearch_keywords() {
        return search_keywords;
    }

    public void setSearch_keywords(List<String> search_keywords) {
        this.search_keywords = search_keywords;
    }

    public List<NordVpnCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<NordVpnCategory> categories) {
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public NordVpnGeoPoint getLocation() {
        return location;
    }

    public void setLocation(NordVpnGeoPoint location) {
        this.location = location;
    }

    public NordVpnFeatures getFeatures() {
        return features;
    }

    public void setFeatures(NordVpnFeatures features) {
        this.features = features;
    }
}
