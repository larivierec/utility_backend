package com.geek.opensource.utility_backend.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserDao extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
