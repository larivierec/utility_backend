package com.geek.opensource.utility_backend.controllers;

import com.geek.opensource.utility_backend.data.nordvpn.NordVpnServerData;
import com.geek.opensource.utility_backend.model.requests.ProxyClosureRequest;
import com.geek.opensource.utility_backend.model.responses.ProxyResponse;
import com.geek.opensource.utility_backend.service.ProxyService;
import com.geek.opensource.utility_backend.service.VpnInterrogatorService;
import com.sun.istack.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/vpn")

public class VpnController {
    private static final Logger logger = LoggerFactory.getLogger(VpnController.class);

    private final VpnInterrogatorService vpnInterrogatorService;
    private final ProxyService proxyService;

    private final String PERMANENT_DNS_NAME = "tunnel.garbinc.ca";

    @Autowired
    public VpnController(VpnInterrogatorService service, ProxyService proxyService) {
        this.vpnInterrogatorService = service;
        this.proxyService = proxyService;
    }

    /**
     * Takes a country filter, service type and server load parameter to returned a filtered list of
     * available nordvpn available servers
     * @param countryFilter, country in the form of "Canada, United States, Mexico"...
     * @param serviceType, "P2P, ProxyResponse"...
     * @param serverLoad, "50, 60, 70" integer form of percentage
     * @return List of nordvpn available servers
     */
    @GetMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NordVpnServerData>> nordVpnServers(@RequestParam(value="country", required = false) String countryFilter,
                                                                  @RequestParam(value="connection_type", required = false) String serviceType,
                                                                  @RequestParam(value="max_load") Integer serverLoad,
                                                                  @RequestParam(value="features", required = false) List<String> features) {
        try {
            CompletableFuture<List<NordVpnServerData>> data = vpnInterrogatorService.getNordVpnServerData();

            if(countryFilter != null && countryFilter.chars().count() > 2) {
                countryFilter = getCountryCodeFromName(countryFilter);
            }

            String finalCountryFilter = countryFilter;
            List<NordVpnServerData> serverLoadFilter = data.get().stream()
                    .filter(element -> element.getLoad() <= serverLoad).collect(Collectors.toList());

            List<NordVpnServerData> filteredData = serverLoadFilter;
            if(countryFilter != null && !countryFilter.isEmpty()) {
                filteredData = serverLoadFilter.stream()
                        .filter(element -> element.getFlag().equalsIgnoreCase(finalCountryFilter)).collect(Collectors.toList());
            }

            if(serviceType != null) {
                filteredData = filteredData.stream()
                        .filter(element -> element.getSearch_keywords().contains(serviceType)
                                && element.getFeatures().isSocks())
                        .collect(Collectors.toList());
            }

            return new ResponseEntity<>(filteredData, HttpStatus.OK);
        } catch (InterruptedException e) {
            logger.error("Interrupted Exception:", e);
            e.printStackTrace();
        } catch (ExecutionException e) {
            logger.error("Execution Exception :", e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("IOException :", e);
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "tunnel", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createTunnel(@RequestParam(value="country") String country,
                                       @RequestParam(value="connection_type") String serviceType) throws InterruptedException, ExecutionException, IOException {
        ProxyResponse availableServer = proxyService.createProxy(country, serviceType);
        availableServer.dnsName = PERMANENT_DNS_NAME;
        return new ResponseEntity<>(availableServer, HttpStatus.OK);
    }

    @DeleteMapping(path = "tunnel")
    public ResponseEntity stopTunnel(@RequestBody @NotNull final ProxyClosureRequest request) {
        proxyService.stopProxy(request.getPort());
        return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }

    public String getCountryCodeFromName(String country) {
        return Arrays.asList(Locale.getISOCountries())
                .stream()
                .map((s) -> new Locale("", s))
                .filter((l) -> l.getDisplayCountry().toLowerCase().equals(country.toLowerCase()))
                .findFirst()
                .map((l) -> l.getCountry().toLowerCase())
                .orElse("UNKNOWN");
    }

}


