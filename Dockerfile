FROM openjdk:8u171-jre-alpine

COPY build/libs/utility_backend_service*.jar /usr/lib/utility_backend_service/app.jar
WORKDIR /usr/lib/utility_backend_service
VOLUME /etc/cfg
EXPOSE 8080 

ENV SPRING_PROFILES_ACTIVE=dev
ENV JAVA_OPTS="-Xms64m -Xmx192m -Xss256k -XX:MaxMetaspaceSize=64m"
ENV CONFIG_PATH=/etc/cfg

ENTRYPOINT exec java $JAVA_OPTS -jar app.jar --Dspring.config.location=$CONFIG_PATH/,classpath:/
CMD ["catalina.sh", "run"]

